
function validatePassword(req, res, next) {
    const { user_password } = req.body;
    const validatePassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d!@#$%^&*()_+]{12,}$/
    if (!validatePassword.test(user_password)) {
        return res.status(401).json({ message: "Le mot de passe doit contenir 12 caractères comprenant des majuscules, des minuscules, des chiffres et des caractères spéciaux." });
    }

    next()
};

module.exports = { validatePassword };