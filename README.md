# Express Template (lite)

- Suis les consignes du [brief de projet](https://gitlab.com/-/snippets/3637617).
- Travaille en [agilité avec le framework SCRUM](https://gitlab.com/-/snippets/3625937).
- Respecte les étapes [du GitLab Flow](https://gitlab.com/-/snippets/3625931).
- Crée [des demandes de fusion et procède à de la relecture de code](https://gitlab.com/-/snippets/3627568).

## Pré-requis

Installer Node.

## Initialisation du projet

Pour installer les dépendances :

```bash
npm ci
```

## Configuration du projet

Dupliquer le fichier `.env.sample` en `.env` et renseigner les valeurs attendues.
