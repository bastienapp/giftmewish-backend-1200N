const express = require('express');
const router = express.Router();
const connection = require("../config/db");

// Afficher les cadeaux d'une liste
router.get('/by-list/:list_id', (req, res) => {
    const listGift = req.params.list_id;

    connection.execute('SELECT * FROM gift WHERE list_id = ?', [listGift], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération des cadeaux de la liste');
        } else {
            res.json(results);
        }
    })
});

// Afficher un cadeau spécifique
router.get('/:gift_id', (req, res) => {
    const giftID = req.params.gift_id;

    connection.execute('SELECT * FROM gift WHERE gift_id = ?', [giftID], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération du cadeau');
        } else {
            res.json(results);
        }
    })
});

// Ajouter des cadeaux
router.post('/users/:id/list/:list_id/gift_id/:gift_id', (req, res) => {
    const { gift_name, gift_description, gift_desire_level, gift_price } = req.body;
    connection.execute('INSERT INTO gift (gift_name, gift_description, gift_desire_level, gift_price) VALUES (?, ?, ?, ?)', [gift_name, gift_description, gift_desire_level, gift_price], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de l\'ajout du cadeau');
        } else {
            res.status(201).send(`Cadeau ajouté à la liste : ${results.list_id}`);
        }
    });
});

// Modifier un cadeau
router.put('/users/:id/gifts/:gift_id', (req, res) => {
    const { gift_name, gift_description, gift_desire_level, gift_price } = req.body;
    const giftID = req.params.gift_id;
    // Mettre à jour le cadeau
    connection.execute('UPDATE gift SET gift_name = ?, gift_description = ?, gift_desire_level = ?, gift_price = ? WHERE gift_id = ?', [giftID, gift_name, gift_description, gift_desire_level, gift_price], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la mise à jour du cadeau');
        } else {
            // Confirmation de la requête
            res.send(`Le cadeau a été mis à jour.`);
        }
    });
});

// S'engager /désengager d'un cadeau
router.put('/users/:id/gifts/set_aside/:gift_id', (req, res) => {
    const { reservation_status } = req.body;
    const giftID = req.params.gift_id;
    // Mettre à jour le cadeau true ou false
    connection.execute('UPDATE gift SET reservation_status = ? WHERE gift_id = ?', [giftID, reservation_status], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la mise à jour du cadeau');
        } else {
            // Confirmation de la requête
            res.send(`Le cadeau a été mis à jour.`);
        }
    });
});

// Retirer un cadeau de la liste
router.delete('/users/:id/gifts/:gift_id', (req, res) => {
    const giftID = req.params.gift_id;
    // Supprimer le Cadeau
    connection.execute('DELETE FROM gift WHERE gift_id = ?', [giftID], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la suppression du Cadeau');
        } else {
            // Confirmation de la requête
            res.send(`Cadeau supprimé.`);
        }
    });
});


module.exports = router;
