const express = require("express");
const router = express.Router();

const connection = require("../config/db.js");

router.get("/", (req, res) => {
  connection.query("SELECT * FROM list l INNER JOIN theme t ON l.theme_id = t.theme_id", (err, results) => {
    if (err) {
      res.status(500).send("Erreur lors de la récupération des listes");
    } else {
      res.json(results);
    }
  });
});

router.get("/:list_id", (req, res) => {
  const id = req.params.list_id;
  connection.execute(
    "SELECT * FROM list WHERE list_id = ?",
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la récupération de la liste");
      } else {
        // Envoi du résultat sous forme de JSON si la requête est réussie
        res.json(results);
      }
    }
  );
});

module.exports = router;
