const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
// const saltRounds = 10;
const connection = require("../config/db.js");
const { validatePassword } = require("../middlewares/validator.middleware.js");

//Afficher les utilisateurs
router.get("/", (req, res) => {
  connection.query("SELECT * FROM user", (err, results) => {
    if (err) {
      res.status(500).send({ error: err.message });
    } else {
      res.json(results);
    }
  });
});

//Afficher les listes d'un utilisateur
router.get("/:id/lists", (req, res) => {
  const userID = req.params.id;
  connection.query(
    "SELECT * FROM list l INNER JOIN user u ON l.user_id = u.user_id WHERE l.user_id = ?",
    [userID],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err.message });
      } else {
        res.json(results);
      }
    }
  );
});

//Afficher une liste particulière d'un utilisateur
router.get("/:id/lists/:list_id", (req, res) => {
  const userID = req.params.id;
  const listID = req.params.list_id;
  connection.query(
    "SELECT * FROM list l LEFT JOIN user u ON l.user_id = u.user_id WHERE l.user_id = ? AND list_id = ? ",
    [userID, listID],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err.message });
      } else {
        res.json(results);
      }
    }
  );
});

//Partager une liste à un utilisateur en fonction de son email
router.get(
  "/:main_user_id/list/:list_id/guest_user/:guest_mail/:guest_id",
  (req, res) => {
    const listID = req.params.list_id;
    const guestUserID = req.params.guest_id;
    const guestUserMail = req.params.guest_mail;

    connection.query(
      `SELECT * FROM user WHERE user_email = ?`,
      [guestUserMail],
      (err, results) => {
        if (err) {
          res.status(500).json({ error: err.message });
        } else if (results.length === 0) {
          res.status(401).send(`L'utilisateur n'a pas été trouvé`);
        } else {
          connection.execute(
            `INSERT INTO user_has_list(list_id, user_id) VALUES(?, ?)`,
            [listID, guestUserID],
            (mySqlError, results) => {
              if (mySqlError) {
                res.status(500).json({ error: mySqlError.message });
              } else {
                res.sendStatus(200);
              }
            }
          );
        }
      }
    );
  }
);

//Afficher les listes qui lui ont été partagées, d'un utilisateur
router.get("/:id/shared_lists", (req, res) => {
  const userID = req.params.id;
  connection.query(
    ` SELECT * FROM list l 
      INNER JOIN user_has_list uhl ON l.list_id = uhl.list_id
      WHERE uhl.user_id = ?
    `,
    [userID],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err.message });
      } else {
        res.json(results);
      }
    }
  );
});

//Modifier une liste
router.put("/:id/lists/:list_id", (req, res) => {
  const listID = req.params.list_id;
  const userID = req.params.id;
  const { list_name, list_description, list_date, theme_id } = req.body;

  connection.execute(
    "UPDATE list LEFT JOIN user ON list.user_id = user.user_id SET list.list_name = ?, list.list_description = ?, list.list_date = ?, list.theme_id = ? WHERE list.list_id = ? AND user.user_id = ?",
    [list_name, list_description, list_date, theme_id, listID, userID],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err.message });
      } else {
        const data = req.body;
        const listUpdated = {
          ...data,
          ...results,
        };
        res.json(listUpdated);
      }
    }
  );
});

// Utiliser Hash en middleware ?
// Valider le mot de passe en le comparant au mdp de la base de donnée

//Ajouter un utilisateur
router.post("/register", validatePassword, (req, res) => {
  let { user_email, user_password, fullName } = req.body;
  const saltRounds = 10;
  // fonction asyncrone
  bcrypt
    // étape 1 hash (password, saltRounds), autogénère le salt
    .hash(user_password, saltRounds)
    // étape 2, pour vérifier que ça marche, console.log du résultat hashé
    .then(function (hash) {
      user_password = hash;
      connection.execute(
        `INSERT INTO user (user_email, user_password, fullName) VALUES (?, ?, ?)`,
        [user_email, user_password, fullName],
        (err, results) => {
          if (err) {
            res.status(500).send({ error: err.message });
          } else {
            console.log(user_password);
            delete req.body.user_password;
            res.send(req.body);
          }
        }
      );
    })
    .catch((err) => console.error(err.message));
});

router.post("/login", (req, res) => {
  let { user_email, user_password } = req.body;
  if (!user_email || !user_password) {
    res.json({
      errorMessage: "l'email et le mot de passe ne peuvent pas être vides!",
    });
  } else {
    connection.query(
      "SELECT * FROM user WHERE user_email = ?",
      [user_email],
      (err, results) => {
        if (err) {
          res.status(500).send("Les informations n'ont pas pu être récupérées");
        } else if (results.length === 0) {
          res.status(401).json({ message: "l'email saisi n'existe pas" });
        } else {
          const user = results[0];
          const hashedPassword = user.user_password;

          bcrypt.compare(
            user_password,
            hashedPassword,
            (bcryptError, passwordMatched) => {
              if (bcryptError) {
                res.status(500).json({ error: bcryptError.message });
              } else if (passwordMatched) {
                res
                  .status(200)
                  .json({ user_id: user.user_id, user_email: user.user_email });
              } else {
                res.status(401).json({ error: "Mot de passe invalide." });
              }
            }
          );
        }
      }
    );
  }
});

router.delete("/:id/lists/:list_id", (req, res) => {
  const list_id = req.params.list_id;

  connection.execute(
    "DELETE FROM list WHERE list_id = ?",
    [list_id],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la suppression de la liste");
      } else {
        res.send(`Liste supprimé.`);
      }
    }
  );
});

//Créer une liste à partir d'un thème
router.post("/:user_id/themes/:theme_id/lists", (req, res) => {
  const { list_name, list_description, list_date } = req.body;
  const userID = req.params.user_id;
  const themeID = req.params.theme_id;

  connection.execute(
    `
  INSERT INTO list (list_name, list_description, list_date, user_id, theme_id)
  VALUES (?, ?, ? , ?, ?)`,
    [list_name, list_description, list_date, userID, themeID],
    (err, results) => {
      if (err) {
        res.status(500).send({ message: err.message });
      } else {
        res.json(req.body);
      }
    }
  );
});

module.exports = router;
