// themes.controller.js
const express = require('express');
const router = express.Router();

// Connexion à la base de données pour faire les requêtes SQL
const connection = require("../config/db");



// Route GET pour afficher tous les thèmes
router.get('/', (req, res) => {
    connection.execute('SELECT * FROM theme', (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération des listes');
        } else {
            // Envoi du résultat sous forme de JSON si la requête est réussie
            res.json(results);
        }
    });
    
});

//Route POST “/themes/default” : default theme
router.post('/default', (req, res) => {
    const defaultThemeRoute = 'SELECT * FROM theme WHERE theme_name = "Par défaut"';
    connection.execute(defaultThemeRoute, (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération de la liste par défaut');
        } else {
            // Envoi du résultat sous forme de JSON si la requête est réussie
            res.json(results);
        }
    });
});

// Route POST “/themes/christmas_list” : noël
router.post('/christmas_list', (req, res) => {
    const christmasThemeRoute = 'SELECT * FROM theme WHERE theme_name = "Noël"';
    connection.execute(christmasThemeRoute, (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération de la liste de Noël');
        } else {
            // Envoi du résultat sous forme de JSON si la requête est réussie
            res.json(results);
        }
    });
});

// Route POST “/themes/birth_list” : naissance
router.post('/birth_list', (req, res) => {
    const birthThemeRoute = 'SELECT * FROM theme WHERE theme_name = "Naissance"';
    connection.execute(birthThemeRoute, (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération des listes de naissance');
        } else {
            // Envoi du résultat sous forme de JSON si la requête est réussie
            res.json(results);
        }
    });
});

// Route POST “/themes/birthday_list” : anniversaire
router.post('/birthday_list', (req, res) => {
    const birthdayThemeRoute = 'SELECT * FROM theme WHERE theme_name = "Anniversaire"';
    connection.execute(birthdayThemeRoute, (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération des listes d\'anniversaire');
        } else {
            // Envoi du résultat sous forme de JSON si la requête est réussie
            res.json(results);
        }
    });
});

// Route POST “/themes/wedding_list” : mariage
router.post('/wedding_list', (req, res) => {
    const weddingThemeRoute = 'SELECT * FROM theme WHERE theme_name = "Mariage"';
    connection.execute(weddingThemeRoute, (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération des listes de mariage');
        } else {
            // Envoi du résultat sous forme de JSON si la requête est réussie
            res.json(results);
        }
    });
});

module.exports = router;
