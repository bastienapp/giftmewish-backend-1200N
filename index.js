require('dotenv').config();
const express = require("express");
const app = express();
const cors = require('cors');
app.use(express.json());
app.use(cors({
  origin: 'http://localhost:8080'
})
);
app.use(
  express.urlencoded({
    extended: true,
  })
);

const giftsController = require('./controllers/gifts.controllers');
app.use('/gifts', giftsController);

const themesController = require('./controllers/themes.controller.js')
app.use('/themes', themesController);

const usersControllers = require('./controllers/users.controller');
app.use('/users', usersControllers);

const listsControllers = require('./controllers/lists.controller');
app.use('/lists', listsControllers);

const port = process.env.PORT || 5000;

app.get("/", (request, response) => {
  response.send("Accueil");
});
app.listen(port, () => {
  console.log(`Server launched on http://localhost:${port}`);
});

